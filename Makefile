build-amd64:
	docker buildx build --push --platform linux/amd64 --tag "$(IMAGE):$(TAG)-amd64" .

build-arm64:
	docker buildx build --push --platform linux/arm64 --tag "$(IMAGE):$(TAG)-amd64" .

build-manifest:
	docker manifest create $(IMAGE):$(TAG) "$(IMAGE):$(TAG)-amd64" "$(IMAGE):$(TAG)-arm64"
	docker manifest push $(IMAGE):$(TAG)

build-platform:
    docker build --tag "$(IMAGE):$(TAG)" --tag "$(IMAGE):latest" .
	docker push "$(IMAGE):$(TAG)"
    docker push "$(IMAGE):latest"
